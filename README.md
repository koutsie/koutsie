# Heya, I'm Koutsie. 
### I don't code.

![Metrics](https://metrics.lecoq.io/koutsie?template=classic&pagespeed=1&pagespeed.url=koutsie.github.io&pagespeed.detailed=false&pagespeed.screenshot=false&config.timezone=Europe%2FHelsinki&config.twemoji=true)

![koutsie's github stats](https://github-readme-stats.vercel.app/api?username=koutsie&show_icons=true&hide_border=true&theme=synthwave)<br>

## IM: 
<!-- #### Please use [PGP](https://koutsie.github.io/pgp.html) if possible. -->


[Telegram](https://t.me/scafizion).

Note, it's better to hit me up elsewhere first and then send a message in Tox.

---

##### Bother me in; [Twitter](https://twitter.com/notkoutsie) or [Mastodon](https://mastodon.technology/@koutsie).
##### Maybe you like [Discord?](https://dsc.bio/ko) or want to be friends in [Steam?](https://steamcommunity.com/id/koutsie/) ([Friend link](https://s.team/p/pvc-bmhq))


[![HitCount](http://hits.dwyl.com/koutsie/koutsie.svg)](http://hits.dwyl.com/koutsie/koutsie)
